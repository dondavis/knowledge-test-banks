FROM registry.gitlab.com/hmajid2301/markdown-mermaid-to-images

ARG USERNAME=node
ARG HOME=/home/$USERNAME

# Cleanup cache folder so apk repos will work
RUN rm -rf /var/cache/apk && mkdir /var/cache/apk
# Since bash is primarily used in our scripts
RUN apk add --update --no-cache bash
# For development purposes
RUN addgroup $USERNAME && adduser -s /bin/bash -G $USERNAME -D $USERNAME
# These repos are necessary for mongodb
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.9/main' >> /etc/apk/repositories
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.9/community' >> /etc/apk/repositories
# Install necessary dependencies and programs
RUN apk add --update --no-cache musl-dev gcc g++ python3-dev freetype-dev jpeg-dev zlib-dev libjpeg \
    sudo git curl wget mongodb yaml-cpp=0.6.2-r2 mongodb-tools
# Setup colored prompt
COPY profile /etc/
COPY color_prompt /etc/profile.d/
RUN mv /etc/profile.d/color_prompt /etc/profile.d/color_prompt.sh
# Setup $USERNAME as a sudoer
RUN echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME
WORKDIR $HOME

COPY requirements.txt .
RUN pip3 install -r requirements.txt

RUN curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash && \
    echo "if [ -f ~/.git-completion.bash ]; then . ~/.git-completion.bash; fi" >> ~/.bashrc

ENV SHELL /bin/bash
