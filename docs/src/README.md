# Knowledge Test Bank System

### Slides are located [here](./slides)

The repository houses the Knowledge Test Bank system as a whole and will act as a template can be used by ourselves and other organizations. This system will include the following.
1. Test Bank Material
2. Change Management
3. Automated deployment of study presentation
4. Manual deployment (in an automated fashion) of official exams

This system will operate using a standard product release life-cycle at which will be orchestrated using GitLab release tags.

### Development Overview of this system can be found on its [GitLab Page](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system)