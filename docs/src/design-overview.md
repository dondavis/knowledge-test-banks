# System Design Overview
In the [conceptual overview](conceptual-overview.md) we talked about the different layers and technologies of this product. Here we're going to talk about how those layer technologies are implemented, abstractly, inside the project.

## 1. Project Directory Structure
- `.devcontainer`: This folder is used to configure the development environment used to maintain the system locally
- `docs`: This folder contains all the markdown files used for product documentation which will be deployed to the public mdbook static page
    - `book.toml`: This file is the configuration file for mdbook and will be used to define title, author, etc.
- `scripts`: This folder contains all the programming files needed to interface/execute/generate the different layers
    - `study-bank-pres`: Contains the study presentation layer programming files
    - `formal-exam-pres`: Contains the formal exam presentation layer programming files
    - `test-bank`: Contains all the test bank layer programming files
- `test-banks`: This folder is where all your test banks will be defined
- `.gitignore`: File used by git to prevent files/folders from being committed to the project
- `.gitlab-ci.yml`: File used by GitLab to define the project's CI/CD pipeline
- `.gitmodules`: File used to define submodules used inside the project
- `docker-compose.yml`: File used to define the development environment used to maintain the project
- `CONTRIBUTE.md`: File used to define how anyone can contribute to the project
- `README.md`: File used to display general information about the product


## The Layers
### Test Bank
The test bank layer is where the exam material (`test-banks`) and the validation scripts for the exam material reside. As discussed in the [conceptual overview](conceptual-overview#test-bank), this layer uses GitLab and GitLab's CI/CD feature to automate the validation of the exam material. The [test_bank_ci.yml](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/blob/master/scripts/test_bank/test_bank_ci.yml) contains the CI/CD [jobs](https://docs.gitlab.com/ee/ci/jobs/index.html) for this layer. Each question in the exam is a simple `JSON` file that contains all the information for that question (`*.question.json` files). These `JSON` files are located in a specific test bank in the `test-banks` folder.

This project is designed to house one or more test banks. Each test bank is defined as a separate folder in the `test-banks` directory. Once the test bank is defined and populated with `question.json` files, the system will do the rest. Any maintenance of the exam material only requires a merge request that changes the `question.json` file for each question that you wish to change.

Validation of the material is performed automatically by the CI/CD pipeline for every `question.json` file each time changes are pushed to any branch.  The validation procedure only include JSON syntax and structure at this time.

More information about this layer and how to set it up is located in the [Test Bank Guide](guides/test-banks.md).

### Study Presentation
The study presentation layer digests the test-bank layer's material and generates the public presentation of the study bank as discussed in the [conceptual overview](conceptual-overview#study-presentation). There are a few things that are happening in the CI/CD pipeline to make this possible. The [mdbook_ci.yml](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/blob/master/scripts/study_bank_pres/mdbook/md_book_ci.yml) defines the pipeline for this layer. 

First, we build the study bank material in the `build_study_bank` [job](https://docs.gitlab.com/ee/ci/jobs/index.html). This dynamically creates the markdown files and configures them for the study bank mdbook. Second, we pass the generated [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) from the `build_study_bank` job and generate the mdbook website in the `pages` job. To publish the static page using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) it requires the `pages` job to place a static web page in a `public` folder and set this folder as an artifact for GitLab to send to their webservers. The `pages` job only runs on the master branch. Once this is finished, the page will be published at the link specified in the project `Settings` --> `Pages` view. The `test_mdbook_pages` is identical to the `pages` job but it does not publish the mdbook web page that is generated and it only runs on merge request branches. 

More information about this layer and how to set it up is located in the [Study Presentation Guide](guides/study-presentation.md).

### WIP: Formal Exam Presentation
The formal exam presentation layer, similar to the study presentation layer, digests the test-bank layer's material and generates the public presentation of a formal exam as discussed in the [conceptual overview](conceptual-overview#formal-exam-presentation). It is designed to be interchangable with whatever formal exam solution the customer needs. Currently, the only option that is available is Google Forms. As of now, there are series of automated and manual steps required in order to produce a formal exam using this option that will be discussed in the guide associated with this layer. The [google_suite_ci.yml](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/blob/master/scripts/formal_exam_pres/google_suite/google_suite_ci.yml) defines the pipeline for this layer.

More information about this layer and how to set it up is located in the [Formal Exam Presentation Guide](guides/formal-exam-presentation.md).