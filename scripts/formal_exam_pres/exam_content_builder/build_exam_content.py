import os
import sys
import copy
import json
import pymongo
import argparse
import datetime
# In order to gain access to modules within the Scripts folder; it shouldn't go beyond scripts
# The first os.path.dirname gets this file's directory; the next one gets its parent
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
from formal_exam_pres.exam_content_builder import eval_builder, eval_question
from formal_exam_pres.exam_content_builder.support import text_to_image
from shared_local.default_strings import topic_key, id_key, snippet_lang_key, snippet_key, question_name_key, \
    topics_key, exams_generated_key, exams_to_gen_key, time_allowed_key, image_name_key, mongo_host_default, mongo_host_var_name, mongo_port_default, mongo_port_var_name

DESCRIPTION = "This will pick questions that will go on an exam."

# Argparse params
ENABLE_MTTL_DESC = "When specified, this enables usage of an MTTL MongoDB connector."
EXAM_GEN_STATS_FILE_DESC = "The path to the file storing the test bank's exam generation statistics."
FONT_FILE_DEFAULT = "support/font_files/monaco.ttf"
FONT_FILE_DESC = f"The path to the font file used during image creation."
LOG_FILE_DEFAULT = "GenKnowledgeReport.json"
LOG_FILE_DESC = f"The name of the report file; the default is '{LOG_FILE_DEFAULT}'."
MONGO_HOST_DEFAULT = "localhost"
MONGO_HOST_DESC = f"This is the host name where the Mongo Database is running. " \
                  f"The default is '{MONGO_HOST_DEFAULT}'."
MONGO_PORT_DEFAULT = "27017"
MONGO_PORT_DESC = f"This is the port where the Mongo database is running. The default is '{MONGO_PORT_DEFAULT}'."
MTTL_DB_NAME_DEFAULT = "mttl"
MTTL_DB_NAME_DESC = "This is the name of the MTTL database and is used for DB connection purposes. " \
                    f"The default is '{MTTL_DB_NAME_DEFAULT}'."
NAMEOF_MQF_FILE_SUFFIX_DEFAULT = "MQF.json"
NAMEOF_MQF_FILE_SUFFIX_DESC = "The suffix name of the Master Question File (MQF); the test bank's name is prepended " \
                              "automatically. For example, a test bank named DEV will generate a file named " \
                              f"'DEV_{NAMEOF_MQF_FILE_SUFFIX_DEFAULT}'. " \
                              f"The default is '{NAMEOF_MQF_FILE_SUFFIX_DEFAULT}'."
OUT_DIR_DEFAULT = "."
OUT_DIR_DESC = "The directory to store the resulting Generate Google Script Files report. " \
               f"The default is '{OUT_DIR_DEFAULT}'."
TESTBANK_DB_NAME_DEFAULT = "knowledge_test_bank"
TESTBANK_DB_NAME_DESC = "This is the name of the test bank database and is used for DB connection purposes. " \
                         f"The default is '{TESTBANK_DB_NAME_DEFAULT}'."
TESTBANK_WORKROLE_NAME_DESC = "This specifies the work role test bank for exam generation; for example, planner, " \
                              "programmer, etc."


def get_test_gen_stats(stats_file: str) -> dict:
    '''
    Purpose: This loads the stored generator statistics data into a dictionary.
    :param stats_file: The path to the file with the stored generator statistics data.
    :return: gen_stats - The stored generator statistics data
    '''
    gen_stats = {}
    try:
        with open(stats_file) as stats:
            gen_stats = json.load(stats)
    except (json.JSONDecodeError, FileNotFoundError) as err:
        raise ValueError(f"Unable to load {stats_file}: {err}")
    return gen_stats


def get_cur_topics(topic_list: list) -> dict:
    '''
    Purpose: This gets a generator statistics dictionary of all current topics in use, according to the input list.
    :param topic_list: The list of questions being considered as possible exam question candidates.
    :return: topics_data - The new generator statistics topics data
    '''
    topics_data = {topics_key: {}}
    for topic in topic_list:
        if topic not in topics_data[topics_key]:
            topics_data[topics_key][topic] = 0
    return topics_data


def update_topics(src_topics: dict, dst_gen_stats: dict) -> None:
    '''
    Purpose: This updates the generator statistics with any new topics, as returned from get_cur_topics().
    :param src_topics: The updated topics discovered in the question list.
    :param dst_gen_stats: The data from the generator statistics file stored in the repository.
    :return: None
    '''
    updated_topics = copy.deepcopy(src_topics)  # Prevent modification of input param src_topics
    if topics_key in updated_topics:
        if topics_key not in dst_gen_stats:
            dst_gen_stats[topics_key] = {}  # Prevents KeyError
        updated_topics[topics_key].update(dst_gen_stats[topics_key])  # Maintains requested values in dst_gen_stats
        dst_gen_stats[topics_key] = copy.deepcopy(updated_topics[topics_key])  # Store result in dst_gen_stats
    else:
        print("WARNING: No topics in source. If this was on purpose, you may ignore this warning.", file=sys.stderr)
    return


def check_old_topics(current_topics: dict, gen_stats: dict) -> None:
    '''
    Purpose: This checks the generator statistics data for any outdated topics.
    :param current_topics: The updated topics discovered in the question list.
    :param gen_stats: The data from the generator statistics file stored in the repository.
    :return: None
    '''
    msgs = []
    if topics_key not in current_topics:
        # In order to avoid getting KeyError when not present
        current_topics[topics_key] = {}
    if topics_key in gen_stats:
        # Evaluate each topic and print a message for any question using an outdated topic
        for topic, num_questions in gen_stats[topics_key].items():
            if topic not in current_topics[topics_key] and num_questions > 0:
                msgs.append(f"WARNING: The exam generation stats file is requesting {num_questions} '{topic}' "
                            "questions; however, there are currently ZERO questions in the test bank using this topic.")
        if len(msgs) > 0:
            print("\n".join(msgs), file=sys.stderr)


def check_unused_category(generator_stats: dict):
    '''
    Purpose: This checks for topics that have zero questions configured for use.
    :param generator_stats: The data from the generator statistics file stored in the repository.
    :return: None
    '''
    msgs = []
    if topics_key in generator_stats:
        # Evaluate each topic and print a message for any requesting ZERO questions
        for topic, num_questions in generator_stats[topics_key].items():
            if num_questions == 0:
                msgs.append(f"WARNING: The exam generation stats file is requesting {num_questions} '{topic}' "
                            f"questions.")
        if len(msgs) > 0:
            print("\n".join(msgs), file=sys.stderr)


def get_exam_json(exams: list) -> str:
    '''
    Purpose: This creates a JSON string of the input exams list.
    :param exams: A list representing one or more exams; each exam contains a list multiple question objects.
    :return: question - a JSON string of the question list
    '''
    json_exams = []
    for exam in exams:
        questions = []
        if type(exam) is list:
            # Supports a list of exams; each exam containing a list of questions
            for question in exam:
                questions.append(question.to_dict())
        if len(questions) > 0:
            json_exams.append(questions)
    return json.dumps(json_exams)


def write_exam_json(exam_json: str, out_file: str):
    '''
    Purpose: Saves the JSON string to disk.
    :param exam_json: A JSON string representing the question picked for an exam.
    :param out_file: The location for the JSON file.
    :return: None
    '''
    with open(out_file, "w") as exam_fp:
        exam_fp.write(exam_json)


def create_images(exam_list: list, out_dir: str, font_file: str):
    '''
    Purpose: When applicable, this creates an image file from each question's snippet.
    :param exam_list: A list of question objects as generated in the EvalBuilder class.
    :param out_dir: The directory to store the generated images; note, a directory called 'code_snippets' is added to
                    the end of this path.
    :param font_file: The path to the font file used during image creation.
    :return: None
    '''
    snip_dir = os.path.join(out_dir, "code_snippets")
    for index, question in enumerate(exam_list):
        if snippet_key in question.question_data:
            # Generate an image file for Google Forms to display and store the image name used back in the question
            if snippet_lang_key in question.question_data and question.question_data[snippet_lang_key] == "mermaid":
                # run mermaid to image if the snippet language is specified as mermaid
                image_name = text_to_image.mermaid_to_image(question.question_data[question_name_key],
                                                            question.question_data[snippet_key],
                                                            question.question_data[topic_key],
                                                            question.question_data[id_key],
                                                            snip_dir)
            else:
                image_name = text_to_image.snippet_image_generator(question.question_data,
                                                                   question.question_data[question_name_key],
                                                                   snip_dir, font_file=font_file)

            exam_list[index].question_data[image_name_key] = image_name


def generate_knowledge(generator_stats: dict, eb: eval_builder.EvalBuilder, nameof_mqf_file: str, out_dir: str,
                       font_file: str) -> str:
    '''
    Purpose: This generates the requested amount of Knowledge exams.
    :param generator_stats: The data from the generator statistics file stored in the repository.
    :param eb: The EvalBuilder object used : for exam generation.
    :param nameof_mqf_file: The name for the output Master Question File.
    :param out_dir: The name of the output folder.
    :param font_file: The path to the font file used during image creation.
    :return: file_names - a str representing the file name of the generated exam
    '''
    eb.clear_categories()  # Start with fresh list of categories
    prev_exams = []  # Store previous exams in case the new exam has the same questions
    total_quest = 0
    eb.time_allowed = generator_stats[time_allowed_key]
    for topic in sorted(generator_stats[topics_key]):
        eb.register_category(topic, generator_stats[topics_key][topic])
        total_quest += generator_stats[topics_key][topic]
    eb.sort_cat_list_by_value()
    for exam in range(eb.exams_to_generate):
        exam_q_list = eb.create_exam(prev_exams)
        eb.feedback_update(exam_q_list)
        # Generate any needed images
        create_images(exam_q_list, out_dir, font_file)
        prev_exams.append(exam_q_list)
    # The exam_ver is added to the file name as a date to distinguish this exam from previously created exams
    exam_ver = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%dT%H:%M:%S:%f")
    mqf_ext = os.path.splitext(nameof_mqf_file)[-1]
    mqf_file_name = f"{os.path.splitext(nameof_mqf_file)[0]}_{exam_ver}{mqf_ext}"
    # Save the exam to disk using the final file name
    write_exam_json(get_exam_json(prev_exams), os.path.join(out_dir, mqf_file_name))
    return mqf_file_name


def main(args: argparse.Namespace):
    '''
    Purpose: This is the starting point for the exam generator.
    :param: args: The arguments collected by argparse.
    :return: None
    '''
    file_name = "Unable_to_build_testbank_data"
    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    # Get Mongo DB connection
    host = os.getenv(mongo_host_var_name, args.host)
    port = int(os.getenv(mongo_port_var_name, args.port))
    client = pymongo.MongoClient(host, port)
    db_testbank = client[args.testbank_db_name]
    db_mttl = None

    if args.enable_mttl:
        db_mttl = client[args.mttl_db_name]

    for collection in db_testbank.list_collection_names():
        if collection == args.testbank_workrole_name:
            exam_gen_stats_file = ""
            for path in args.exam_gen_stats_file:
                if collection.lower() in path:
                    exam_gen_stats_file = path
            q_list = list(db_testbank[collection].find())
            if len(q_list) == 0:
                return f"{args.testbank_workrole_name}_testbank_had_no_questions"
            eb = eval_builder.EvalBuilder()
            # Build fresh stat map with current topics
            current_topics = get_cur_topics(list(db_testbank[collection].find().distinct(topic_key)))
            generator_stats = get_test_gen_stats(exam_gen_stats_file)
            # Send warning for old topics being used; these topics won't have questions for them
            check_old_topics(current_topics=current_topics, gen_stats=generator_stats)
            update_topics(src_topics=current_topics, dst_gen_stats=generator_stats)
            # Send warning for topics config'd with 0 questions
            check_unused_category(generator_stats)

            eb.set_question_list(q_list, db_mttl)
            eb.exams_to_generate = generator_stats[exams_to_gen_key]
            file_name = generate_knowledge(generator_stats, eb,
                                           "_".join([collection, args.nameof_mqf_file_suffix]),
                                           args.out_dir, font_file=args.font_file)
            generator_stats[exams_generated_key] += eb.exams_generated
            try:
                with open(exam_gen_stats_file, "w") as stats_f:
                    json.dump(generator_stats, stats_f, indent=4)
            except (json.JSONDecodeError, FileNotFoundError) as err:
                print(f"Unable to save updated generator stats to {exam_gen_stats_file}: {err}.", file=sys.stderr)

    return file_name


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument("exam_gen_stats_file", nargs="+", type=str, help=EXAM_GEN_STATS_FILE_DESC)
    parser.add_argument("testbank_workrole_name", type=str, help=TESTBANK_WORKROLE_NAME_DESC)
    parser.add_argument("font_file", type=str, help=FONT_FILE_DESC)
    parser.add_argument("--enable_mttl", action="store_true", help=ENABLE_MTTL_DESC)
    parser.add_argument("--host", type=str, default=MONGO_HOST_DEFAULT, help=MONGO_HOST_DESC)
    parser.add_argument("--log_file", type=str, default=LOG_FILE_DEFAULT, help=LOG_FILE_DESC)
    parser.add_argument("--mttl_db_name", type=str, default=MTTL_DB_NAME_DEFAULT, help=MTTL_DB_NAME_DESC)
    parser.add_argument("--nameof_mqf_file_suffix", type=str, default=NAMEOF_MQF_FILE_SUFFIX_DEFAULT, 
                        help=NAMEOF_MQF_FILE_SUFFIX_DESC)
    parser.add_argument("--out_dir", type=str, default=OUT_DIR_DEFAULT, help=OUT_DIR_DESC)
    parser.add_argument("--port", type=str, default=MONGO_PORT_DEFAULT, help=MONGO_PORT_DESC)
    parser.add_argument("--testbank_db_name", type=str, default=TESTBANK_DB_NAME_DEFAULT, help=TESTBANK_DB_NAME_DESC)
    params = parser.parse_args()

    print(os.path.join(params.out_dir, main(params)))
