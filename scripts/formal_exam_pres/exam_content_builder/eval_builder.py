import os
import sys
import copy
import pymongo
from colorama import Fore
# In order to gain access to modules within the Scripts folder; it shouldn't go beyond scripts
# The first os.path.dirname gets this file's directory; the next one gets its parent
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
from shared_local.default_strings import topic_key, provisioned_key, attempts_key, passes_key, etc_key, \
    rellink_id_key, id_key, question_type_key, ksats_key, points_key, disabled_key
from formal_exam_pres.exam_content_builder import question_category, eval_question
from shared.lib import mongo_db_helpers


class EvalBuilder:
    def __init__(self):
        self.question_list = []
        self.category_list = []
        self.ksat_set = set()
        self.exams_generated = 0
        self.exams_to_generate = 0
        self.time_allowed = 0
        self.delta = 0  # The difference between the amount of questions generated and requested, as a percentage

    def create_exam(self, prev_exams_list: list = None):
        ex_list = []
        time_used = 0
        if prev_exams_list is None:
            prev_exams_list = []
        for cat in self.category_list:
            cat_list, time_used = self.get_category_questions(cat[0], cat[1], self.time_allowed, time_used,
                                                              rand_list=True)
            ex_list += cat_list

        # return [x.question_data for x in ex_list]
        return ex_list

    def register_category(self, category_name, amount):
        self.category_list.append((category_name, amount))

    def get_category_questions(self, category_name, amount, time_allowed: int, time_used: int, rand_list: bool = False):
        # check for category
        category = question_category.QuestionCategory(category_name, amount)
        # get top amount of questions and return them
        for question in self.question_list:
            if question.question_data[topic_key] == category_name and question.question_data[disabled_key] is not True:
                category.question_list.append(question)
        if rand_list:
            exam, time_used = category.create_rand_quest_set(amount, time_allowed, time_used)
        else:
            exam, time_used = category.create_question_set(amount, time_allowed, time_used)
        if amount - len(exam) > self.delta * amount:
            # If the difference is greater than the allowed delta
            msg = f"Delta between requested size '{amount}' and generated size '{len(exam)}' ({amount - len(exam)}) " \
                  f"> '{self.delta * amount}'"
            raise ValueError(msg)
        return exam, time_used

    def sort_cat_list_by_value(self):
        self.category_list = sorted(self.category_list, key=lambda category: category[1])

    def clear_categories(self):
        self.category_list = []

    def feedback_update(self, exam_q_list):
        self.exams_generated += 1
        for q_id in exam_q_list:
            for q in self.question_list:
                if q_id == q.question_data[id_key]:
                    q.question_data[provisioned_key] += 1
                    # update attempts
                    # update failures

        # reset weights to normal
        for q in self.question_list:
            q.get_question_weight(self.exams_generated)

    def set_question_list(self, questions: list, mttl_db_obj: pymongo.database.Database = None):
        q_list = []

        for question in questions:
            if question[disabled_key] is True:
                # Because we don't want any disabled questions on the exam
                continue
            
            rellink_id = None
            if rellink_id_key in question:
                rellink_id = question[rellink_id_key].strip()
            
            if rellink_id == "":
                rellink_id = None

            if rellink_id is None and rellink_id_key in question:
                print(f"{Fore.YELLOW}The MTTL MongoDB rel-link ObjectID for "
                      f"'{Fore.CYAN + question[id_key] + Fore.YELLOW}' cannot be blank.{Fore.RESET}")
                continue

            time_to_complete = .9  # Expressed in min
            if rellink_id is not None and mttl_db_obj is not None:
                q_rellink_data = mongo_db_helpers.get_question(rellink_id, mttl_db_obj)
                ksats = mongo_db_helpers.get_question_ksats(rellink_id, mttl_db_obj)
                q_rellink_data[ksats_key] = ksats
                # Update question data so the values in question are preserved when keys are the same
                q_rellink_data.update(question)
                question = copy.deepcopy(q_rellink_data)

            if etc_key not in question:
                question[etc_key] = time_to_complete

            question[question_type_key] = "CHOICE"
            question[points_key] = 1

            new_q = eval_question.EvalQuestion(question)

            new_q.get_question_weight(self.exams_generated)
            q_list.append(new_q)

        self.question_list = q_list

    def get_question(self, q_id: str) -> eval_question.EvalQuestion:
        for question in self.question_list:
            if q_id == question.question_data[id_key]:
                return question

    def get_question_list_dict(self) -> list:
        questions = []
        for question in self.question_list:
            questions.append(question.to_dict())
        return questions
