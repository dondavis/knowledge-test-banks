import os
import sys
import copy
# In order to gain access to modules within the Scripts folder; it shouldn't go beyond scripts
# The first os.path.dirname gets this file's directory; the next one gets its parent
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
from shared_local.default_strings import attempts_key, passes_key, provisioned_key, weight_key, q_weight_scalar_key


class EvalQuestion:
    def __init__(self, question: dict = None):
        self.question_data = {} if question is None else copy.deepcopy(question)
        self.weight = 1.0
        self.q_weight_scalar = 1.0

    def get_accuracy(self):
        '''
        :return: A percentage representing the pass rate of this question
        '''
        return max(self.question_data[passes_key], 1) / max(self.question_data[attempts_key], 1)

    # returns the reciprocal of the ratio between the number of tests and the number of times
    # the question has been used in a test
    # fewer uses_test should return higher weights
    def get_question_weight(self, exams_generated: int):
        # simplification of 1 / (uses_test / exams_generated)
        # 1 / (uses_test / exams_generated) = exams_generated / uses_test
        # exams_generated is the number of exams created since the beginning
        # uses_test is the number of times the question was provisioned on an exam; this is different from times
        # attempted
        self.weight = self.q_weight_scalar * (max(exams_generated, 1) / (max(self.question_data[provisioned_key], 1)))
        return self.weight

    def to_dict(self):
        self.question_data[weight_key] = self.weight
        self.question_data[q_weight_scalar_key] = self.q_weight_scalar
        return self.question_data
