#!/usr/bin/python3

import os
import os.path
import sys
import pickle
import argparse
import datetime
import subprocess
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload

DESCRIPTION = "This will upload necessary files for the Google Form Creator, used to generate exams."
DST_SNIP_DIR_ID_DESC = "This is the Google Drive ID of the folder containing all the image files. The ID can be " \
                       f"found in the folder's URL."
IMAGE_EXT_DEFAULT = "png"
IMAGE_EXT_DESC = f"This is the image file extension. The default is '{IMAGE_EXT_DEFAULT}'."
MSEC_DEFAULT = 0
MSEC_DESC = "This is a filter option to return the number of files created within X microseconds." \
            f"The default is '{MSEC_DEFAULT}'"
MIN_DEFAULT = 1
MIN_DESC = "This is a filter option to return the number of files created within X minutes." \
           f"The default is '{MIN_DEFAULT}'"
NAME_OF_HTML_FILE_DESC = "The name of the HTML file that will be generated."
OUT_DIR_DEFAULT = "FormLinks"
OUT_DIR_DESC = "This is the name of the folder where output files will be saved."
SCOPES_DEFAULT = [
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/drive.file"
]
SCOPES_DESC = f"The needed scopes for accessing Google API services. The default is '{SCOPES_DEFAULT}'."
SEC_DEFAULT = 30
SEC_DESC = "This is a filter option to return the number of files created within X seconds." \
           f"The default is '{SEC_DEFAULT}'"
SRC_SNIP_DIR_DEFAULT = "code_snippets"
SRC_SNIP_DIR_DESC = "This is the folder containing all images, needed for the questions in the MQL, to be uploaded. " \
                    f"The default is '{SRC_SNIP_DIR_DEFAULT}'."
TOKEN_PATH_DESC = "This is the token needed for access to Google Drive to allow pushing images."


def delete_all_images(service, dst_snip_dir_id: str, image_ext: str):
    page_token = None
    while True:
        results = service.files().list(q=f"'{dst_snip_dir_id}' in parents and mimeType='image/{image_ext}'",
                                       spaces='drive',
                                       fields='nextPageToken, files(id, name)',
                                       pageToken=page_token).execute()
        for file in results.get('files', []):
            # Process change
            print(f"Deleting file: {file.get('name')} {file.get('id')}", file=sys.stderr)
            try:
                service.files().delete(fileId=file.get('id')).execute()
            except Exception as e:
                print(e, file=sys.stderr)
                return 1
        page_token = results.get('nextPageToken', None)
        if page_token is None:
            break


def get_date_filter(min: int = 1, sec: int = 30, msec: int = 0) -> str:
    date = datetime.datetime.now(datetime.timezone.utc)
    date_diff = datetime.timedelta(minutes=min, seconds=sec, microseconds=msec)
    return datetime.datetime.strftime(date - date_diff, "%Y-%m-%dT%H:%M:%S")


def get_html_file_by_name(service, file_name: str, date_filter: str) -> list:
    files = []
    page_token = None
    query_str = f"createdTime > '{date_filter}' and mimeType='text/html' and trashed = false"
    # query_str = f"mimeType='text/html' and trashed = false"
    while True:
        q_fields = 'nextPageToken, files(id, name, parents, webContentLink, createdTime)'
        results = service.files().list(q=query_str,
                                       spaces='drive',
                                       fields=q_fields,
                                       pageToken=page_token).execute()
        for html_file in results.get('files', []):
            if file_name == html_file.get('name'):
                files.append(html_file)
        page_token = results.get('nextPageToken', None)
        if page_token is None:
            break
    return files


def get_download_urls(files: list) -> dict:
    urls = {}
    for this_file in files:
        if this_file.get('name') not in urls:
            urls[this_file.get('name')] = []
        urls[this_file.get('name')].append(this_file.get('webContentLink'))
    return urls


def save_download_urls(service, out_dir: str, file_name: str, min: int = 1, sec: int = 30, msec:int = 0):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    date_filter = get_date_filter(min, sec, msec)
    for name, urls in get_download_urls(get_html_file_by_name(service, file_name, date_filter)).items():
        unique_id = 1
        for url in urls:
            name = os.path.splitext(file_name)
            name = name[0] + str(unique_id) + name[1]
            unique_id += 1
            output = subprocess.check_output(["curl", "-L", "-o", os.path.join(out_dir, name), url])
            print(output, sys.stderr)


def delete_images(service, images: list):
    for image in images:
        service.files().delete(fileId=image.get('id')).execute()


def get_gdrive_images(service, dst_snip_dir_id: str, image_ext: str):
    images = {}
    page_token = None
    while True:
        results = service.files().list(q=f"'{dst_snip_dir_id}' in parents and mimeType='image/{image_ext}'",
                                       spaces='drive',
                                       fields='nextPageToken, files(id, name, parents)',
                                       pageToken=page_token).execute()
        for file in results.get('files', []):
            # Build list of current images
            if file.get('name') not in images:
                images[file.get('name')] = []
            images[file.get('name')].append(file)

        page_token = results.get('nextPageToken', None)
        if page_token is None:
            break
    return images


def upload(service, root, file, subject, google_folder, image_ext: str):
    file_metadata = {'name': file, 'mimeType': f"image/{image_ext}",
                     'parents': [google_folder]}
    media = MediaFileUpload(os.path.join(root, file))
    file = service.files().create(body=file_metadata, media_body=media).execute()
    print(f"Uploaded snippet '{file.get('name')}' with an ID of '{file.get('id')}' for {subject}", file=sys.stderr)
    return 0


def find_path(file):
    for root, dirs, files in os.walk('.'):
        for name in files:
            if name == file:
                return os.path.abspath(os.path.join(root, name))


def authorize(args: argparse.Namespace):
    creds = None

    token_path = find_path(os.path.basename(args.token_path))
    
    if os.path.exists(token_path):
        with open(token_path, 'rb') as token:
            creds = pickle.load(token)
    else:
        print("User authentication token missing", file=sys.stderr)
        return 1

    return creds


def upload_snippets(service, src_snip_dir: str, dst_snip_dir_id: str, image_ext: str):
    try:
        current_images = get_gdrive_images(service, dst_snip_dir_id, image_ext)
        files_uploaded = 0
        # Look through the local copy of code snippets for .pngs
        for root, dirs, files in os.walk(src_snip_dir):
            subject = os.path.basename(root)
            if "" == subject:
                subject = "Unknown Subject"
            for file in files:
                if file.endswith(".{ext}".format(ext=image_ext)):
                    if file in current_images:
                        # Remove current images so they can be updated
                        delete_images(service, current_images[file])
                    upload(service, root, file, subject, dst_snip_dir_id, image_ext)
                    files_uploaded += 1
        if files_uploaded > 0:
            print(f"Uploaded {files_uploaded} code_snippet(s)", file=sys.stderr)
        else:
            print("No snippets to upload.", file=sys.stderr)
    except Exception as e:
        print(e, file=sys.stderr)
        return 1


def main(args: argparse.Namespace):
    # Get credentials
    creds = authorize(args)

    # Create the drive service to upload and ensure it's closed when done processing
    drive_service = build('drive', 'v3', credentials=creds)
    
    if args.upload_image:
        # Upload the snippets
        upload_snippets(drive_service, src_snip_dir=args.src_snip_dir, dst_snip_dir_id=args.dst_snip_dir_id, 
                        image_ext=args.image_ext)
    
    if args.get_download_urls:
        if os.path.splitext(args.name_of_html_file)[1].lower() != ".html":
            # Ensure the requested file ends in .html
            args.name_of_html_file += ".html"
        save_download_urls(drive_service, args.out_dir, args.name_of_html_file, args.min, args.sec, args.msec)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument("token_path", type=str, help=TOKEN_PATH_DESC)
    parser.add_argument("dst_snip_dir_id", type=str, help=DST_SNIP_DIR_ID_DESC)
    parser.add_argument("name_of_html_file", type=str, help=NAME_OF_HTML_FILE_DESC)
    parser.add_argument("--get_download_urls", action="store_true")
    parser.add_argument("--image_ext", type=str, default=IMAGE_EXT_DEFAULT, help=IMAGE_EXT_DESC)
    parser.add_argument("--min", type=int, default=MIN_DEFAULT, help=MIN_DESC)
    parser.add_argument("--msec", type=int, default=MSEC_DEFAULT, help=MSEC_DESC)
    parser.add_argument("--out_dir", type=str, default=OUT_DIR_DEFAULT, help=OUT_DIR_DESC)
    parser.add_argument("--scopes", nargs='+', type=str, default=SCOPES_DEFAULT, help=SCOPES_DESC)
    parser.add_argument("--sec", type=int, default=SEC_DEFAULT, help=SEC_DESC)
    parser.add_argument("--src_snip_dir", type=str, default=SRC_SNIP_DIR_DEFAULT, help=SRC_SNIP_DIR_DESC)
    parser.add_argument("--upload_image", action="store_true")
    params = parser.parse_args()
    if not params.upload_image and not params.get_download_urls:
        # Allow image upload by default if nothing is specified
        params.upload_image = True
    main(params)
