# The default question JSON keys
answer_key = "answer"
attempts_key = "attempts"
choices_key = "choices"
created_on_key = "created_on"
description_key = "description"
disabled_key = "disabled"
etc_key = "estimated_time_to_complete"
explanation_key = "explanation"
failures_key = "failures"
id_key = "_id"
image_name_key = "image_name"
ksats_key = "KSATs"
ksats_id_key = "ksat_id"
ksats_oid_key = "object_id"
group_key = "group"
group_name_key = "group_name"
group_questions_key = "questions"
passes_key = "passes"
proficiency_key = "proficiency"
provisioned_key = "provisioned"
question_key = "question"
question_name_key = "question_name"
question_path_key = "question_path"
question_prof_key = "question_proficiency"
question_type_key = "question_type"
q_weight_scalar_key = "q_weight_scalar"
rellink_id_key = "rel-link_id"
rev_date_key = "revision_date"
rev_num_key = "revision_number"
snippet_key = "snippet"
snippet_lang_key = "snippet_lang"
topics_key = "topics"
topic_key = "topic"
topic_path_key = "topic_path"
updated_on_key = "updated_on"
weight_key = "weight"
workroles_key = "work-roles"

# MONGO DB strings
mongo_db_name_default = "knowledge_test_bank"
mongo_host_default = "localhost"
mongo_host_var_name = "MONGO_HOST"
mongo_port_default = "27017"
mongo_port_var_name = "MONGO_PORT"

# Exam Generator strings
delta_key = "delta"
exams_generated_key = "exams_generated"
exams_to_gen_key = "exams_to_gen"
points_key = "points"
time_allowed_key = "time_allowed"

# Test Bank strings
test_banks_path_default = "test-banks"

# The standardized name for the output README.md file
readme = "README.md"

# To facilitate printing error and warning text with different colors
error_str = "ERROR"  # Should be formatted for red
warning_str = "WARNING"  # Should be formatted for yellow

# The default usage statement parameter names and descriptions used across all modules
keyword_default = ".question.json"
keyword_desc = "A filter to specify the returned file paths for processing. " \
               "The default is '{0}'.".format(keyword_default)
opt_param_changed_desc = "Gets questions that have changed since the last commit. Without this flag, all JSON file's " \
                         "are retrieved."
param_question_dir_desc = "This is the name of the folder(s) where the '*{0}' files are stored and needs to be " \
                          "relative to the value specified in 'root_dir'. You may specify more than one folder using " \
                          "space delimited values. By default the system collects a list of directories inside the " \
                          "specified 'root_dir' parameter.".format(keyword_default)
param_root_dir_default = "."
param_root_dir_desc = "This is the URL to the target GitLab repository (note, this may be relative). " \
                      "The default is '{}'.".format(param_root_dir_default)
param_md_slide_dirs = "This is the name of the folder(s) where the MD books or slides are stored."
