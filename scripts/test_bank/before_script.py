#!/usr/bin/python3

import os
import sys
import json
import subprocess
import pymongo
# In order to gain access to modules within the Scripts folder; it shouldn't go beyond scripts
# The first os.path.dirname gets this file's directory; the next one gets its parent
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from shared_local.default_strings import question_name_key, question_path_key, question_type_key, provisioned_key, \
    disabled_key, attempts_key, passes_key, failures_key, keyword_default, test_banks_path_default, \
    mongo_db_name_default, mongo_host_default, mongo_host_var_name, mongo_port_default, mongo_port_var_name

HOST = os.getenv(mongo_host_var_name, mongo_host_default)
PORT = int(os.getenv(mongo_port_var_name, mongo_port_default))
db_name = mongo_db_name_default
test_banks_path = test_banks_path_default
client = pymongo.MongoClient(HOST, PORT)
db = client[db_name]

def stage_question(question_data:dict, file_path:str):
    file_name = os.path.basename(file_path)
    question_name = file_name.replace(keyword_default, '')
    if question_name_key not in question_data:
        question_data[question_name_key] = question_name
    if question_path_key not in question_data:
        question_data[question_path_key] = file_path
    if question_type_key not in question_data:
        question_data[question_type_key] = 'knowledge'
    if disabled_key not in question_data:
        question_data[disabled_key] = False
    if provisioned_key not in question_data:
        question_data[provisioned_key] = 0
    if attempts_key not in question_data:
        question_data[attempts_key] = 0
    if passes_key not in question_data:
        question_data[passes_key] = 0
    if failures_key not in question_data:
        question_data[failures_key] = 0

def import_file_mongo(tb_name:str, file_path:str):
    with open(file_path) as question_file:
        question_data = json.load(question_file)
        stage_question(question_data, file_path)
        db[tb_name].insert_one(question_data)

def drop_mongo_db():
    cmd = f'mongo --host {HOST} --eval \'db.dropDatabase()\' {db_name}'
    subprocess.run(cmd, shell=True)

def main():
    drop_mongo_db()
    test_bank_dirs = [f.path for f in os.scandir(test_banks_path) if f.is_dir()]
    for test_bank_dir in test_bank_dirs:
        test_bank_name = os.path.basename(test_bank_dir)
        for root, dirs, files in os.walk(test_bank_dir):
            for name in files:
                if keyword_default in name:
                    # continue
                    import_file_mongo(test_bank_name, os.path.join(root, name))

if __name__ == "__main__":
    main()

